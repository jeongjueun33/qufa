- 부산대학교
- 데이터 공정성팀
- demo1.docker: Jupyter Notebook Demo
- v1.0: 데이터보정모듈(Data compensation) 및 공정성지표(Google Colaboratory Notebook file)
- v2.0: 데이터 공정성 보정 및 개선 검증 모듈(Data fairness improvement)(Jupyter Notebook file)
- v3.0: GAN 기반의 모조 데이터 통합에 따른 보정 알고리즘 및 개선 검증을 위한 모델 고도화(Data fairness improvement v3)(Jupyter Notebook file)
